package ru.kupivip.app;

import jline.console.ConsoleReader;

class Watcher extends Thread {

	private Worker worker;


	Watcher(Worker worker){
		super();
		this.worker = worker;
		start();
	}

	public void run() {

		try {
			ConsoleReader consoleReader = new ConsoleReader();

			while (true){
				int code = consoleReader.readCharacter();
				if(code == 113){
					worker.interrupt();
					break;
				}
				Thread.sleep(100);
			}
		} catch (Exception e){
			System.out.print(("\rError " + e.getMessage()));
		}
	}
}
