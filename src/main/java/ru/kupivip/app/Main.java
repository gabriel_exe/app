package ru.kupivip.app;

import java.io.*;

public class Main {

	public static void main(String[] args){

		if( (args.length < 3 || args.length > 4) || (!args[0].equals("-cp") && !args[0].equals("-mv")) ){
			System.out.println("Invalid arguments. Use: -cp/-mv, [-zip], in.file, out.file");
			System.exit(0);
		}

		String action = args[0];
		boolean zip = (args[1].equals("-zip"));
		String sourceName = zip ? args[2] : args[1];
		String destinationName = zip ? args[3] : args[2];

		File inFile = new File(sourceName);
		File outFile = new File(destinationName);

		if(!inFile.exists()){
			System.out.println("File " + sourceName + " not found");
			System.exit(0);
		}

		Worker worker = new Worker(action, zip, inFile, outFile);
		new Watcher(worker);
		worker.start();
	}
}