package ru.kupivip.app;

import java.io.*;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.FileChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


class Worker extends Thread {

	private String action;
	private boolean zip;
	private File inFile;
	private File outFile;

	Worker(String action, boolean zip, File inFile, File outFile){

		super();

		this.action = action;
		this.zip = zip;
		this.inFile = inFile;
		this.outFile = outFile;
	}

	@Override
	public void run() {

		if(action.equals("-mv") && !zip){

			try(
				FileChannel in = new FileInputStream(inFile).getChannel();
				FileChannel out = new FileOutputStream(outFile).getChannel();
			){
				long transferred = in.size() > 100 ? in.size() / 100 : in.size();
				long start = 0;
				while ( !this.isInterrupted() && start < in.size() ) {
					in.transferTo(start, transferred, out);
					if(!this.isInterrupted()){
						System.out.write(("\rProgress: " + (int)(((double)(start += transferred)/(double)in.size())*100) + " %").getBytes());
					}
				}
			} catch (ClosedByInterruptException e){
				//
			} catch (Exception e){
				System.out.print(("\rError " + e.getMessage()));
			}

		} else {

			try (FileInputStream in = new FileInputStream(inFile);
				 OutputStream out = (!zip)
					 ? new BufferedOutputStream(new FileOutputStream(outFile))
					 : new ZipOutputStream(new FileOutputStream(outFile));
			){
				if(zip){
					((ZipOutputStream) out).putNextEntry(new ZipEntry(inFile.getName()));
				}

				long length = in.available();
				long counter = 0;
				int bytes;
				byte[] buffer = new byte[(int) (length > 100 ? length / 100 : length)];
				while (!this.isInterrupted() && (bytes = in.read(buffer)) > 0){
					out.write(buffer, 0, bytes);
					if(!this.isInterrupted()){
						System.out.write(("\rProgress: " + (int)(((double)(counter+=bytes)/(double)length)*100) + " %").getBytes());
					}
				}
			} catch (Exception e){
				System.out.print(("\rError " + e.getMessage()));
			}
		}

		if(!this.isInterrupted()){

			if(action.equals("-mv")){
				inFile.deleteOnExit();
			}

			String result = (action.equals("-mv"))
				? "\nFile moved" + ((zip) ? " with zip" : "")
				: "\nFile copied" + ((zip) ? " with zip" : "");

			System.out.println(result);
		} else {
			System.out.println("\nCanceled");
			outFile.deleteOnExit();
		}

		System.exit(0);
	}
}
